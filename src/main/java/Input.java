
import java.util.Scanner;

import static com.ulan.Calc.calculate;

public class Input {
    public static void main(String[] args) {

        System.out.println("Введите арифметическую операцию");
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        System.out.println(calculate(line));

    }
}
