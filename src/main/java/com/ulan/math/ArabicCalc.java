package com.ulan.math;

import com.ulan.exeption.MyException;

import java.util.ArrayList;

public class ArabicCalc {
    public static Integer operation(ArrayList<String> list) throws MyException {
        if ("+".equals(list.get(2))) {
            return Integer.parseInt(list.get(0)) + Integer.parseInt(list.get(1));
        }
        if ("-".equals(list.get(2))) {
            return Integer.parseInt(list.get(0)) - Integer.parseInt(list.get(1));
        }
        if ("*".equals(list.get(2))) {
            return Integer.parseInt(list.get(0)) * Integer.parseInt(list.get(1));
        }
        if ("/".equals(list.get(2))) {
            return Integer.parseInt(list.get(0)) / Integer.parseInt(list.get(1));
        } else throw new MyException("Знак не принадлежит к разрешенным арифметическим операциям");//Технически никогда не будет выполнено, т.к. проверяется еще при парсинге
    }
}
