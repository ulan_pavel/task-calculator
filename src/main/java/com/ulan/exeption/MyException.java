package com.ulan.exeption;

public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
