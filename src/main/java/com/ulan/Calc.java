package com.ulan;

import com.ulan.exeption.MyException;
import com.ulan.math.ArabicCalc;
import com.ulan.math.RomanCalc;

import java.util.ArrayList;

import static com.ulan.check.CheckNumber.check;
import static com.ulan.parser.ParseString.parse;

public class Calc {
    public static String calculate(String s){
        try {
            ArrayList<String> list = parse(s);

            if (check(list).get(3).equals("roman")) {
                return RomanCalc.operation(list);
            } else {
                if (check(list).get(3).equals("arabic")) {
                    return ArabicCalc.operation(list).toString();
                }
            }
        } catch (MyException exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
