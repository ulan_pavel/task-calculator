package com.ulan;

import com.ulan.exeption.MyException;
import org.junit.Assert;
import org.junit.Test;

import static com.ulan.Calc.calculate;
import static org.junit.Assert.*;

public class CalcTest {

    @Test
    public void calculateTest() {
        String input = "1+2";
        String input2="5/3";
        String input3 = "1-2";
        String input4 = "10*10";
        String input5 = "I+V";
        String input6 = "V-X";
        String input7 = "VI/III";
        String input8 = "X*V";

        assertEquals("3",calculate(input));
        assertEquals("1",calculate(input2));
        assertEquals("-1",calculate(input3));
        assertEquals("100",calculate(input4));
        assertEquals("VI",calculate(input5));
        assertEquals("-V",calculate(input6));
        assertEquals("II",calculate(input7));
        assertEquals("L",calculate(input8));

    }
}